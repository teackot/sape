#pragma once

#include "Flags.h"

struct Hardware {
    Hardware();
    ~Hardware();

    unsigned char *memory;
    const int memory_size = 65536;

    unsigned short *registers;
    const int registers_number = 16;

    Flags flags;
};

#include "Sape.h"

#include "instructions/Nop.h"
#include "instructions/Halt.h"
#include "instructions/Debug.h"
#include "instructions/Put.h"
#include "instructions/Mov.h"
#include "instructions/Ldr.h"
#include "instructions/Str.h"
#include "instructions/Push.h"
#include "instructions/Pop.h"
#include "instructions/Add.h"
#include "instructions/Sub.h"
#include "instructions/Mul.h"
#include "instructions/Div.h"
#include "instructions/Inc.h"
#include "instructions/Dec.h"
#include "instructions/Shl.h"
#include "instructions/Shr.h"
#include "instructions/And.h"
#include "instructions/Or.h"
#include "instructions/Xor.h"
#include "instructions/Not.h"
#include "instructions/Cmp.h"
#include "instructions/Jmp.h"
#include "instructions/Brl.h"
#include "instructions/Bre.h"
#include "instructions/Brg.h"

Sape::Sape() {
    instr_number = 0x19 + 1;
    instr_registry = new instr::Instruction * [instr_number];
    instr_registry[0x00] = new instr::Nop();
    instr_registry[0x01] = new instr::Halt();
    instr_registry[0x02] = new instr::Debug();
    instr_registry[0x03] = new instr::Put();
    instr_registry[0x04] = new instr::Mov();
    instr_registry[0x05] = new instr::Ldr();
    instr_registry[0x06] = new instr::Str();
    instr_registry[0x07] = new instr::Push();
    instr_registry[0x08] = new instr::Pop();
    instr_registry[0x09] = new instr::Add();
    instr_registry[0x0a] = new instr::Sub();
    instr_registry[0x0b] = new instr::Mul();
    instr_registry[0x0c] = new instr::Div();
    instr_registry[0x0d] = new instr::Inc();
    instr_registry[0x0e] = new instr::Dec();
    instr_registry[0x0f] = new instr::Shl();
    instr_registry[0x10] = new instr::Shr();
    instr_registry[0x11] = new instr::And();
    instr_registry[0x12] = new instr::Or();
    instr_registry[0x13] = new instr::Xor();
    instr_registry[0x14] = new instr::Not();
    instr_registry[0x15] = new instr::Cmp();
    instr_registry[0x16] = new instr::Jmp();
    instr_registry[0x17] = new instr::Brl();
    instr_registry[0x18] = new instr::Bre();
    instr_registry[0x19] = new instr::Brg();

    next_instr = 0;

    // test program

    // put r0, 2
    hard.memory[0x0000] = 0x03 << 2;
    hard.memory[0x0001] = 0;
    hard.memory[0x0002] = 2;

    // put r1, 1
    hard.memory[0x0003] = 0x03 << 2;
    hard.memory[0x0004] = 1 << 4;
    hard.memory[0x0005] = 1;

    // cmp r0, r1
    hard.memory[0x0006] = 0x15 << 2;
    hard.memory[0x0007] = (0 << 4) | 1;

    // put r0, .finish
    hard.memory[0x0008] = 0x03 << 2;
    hard.memory[0x0009] = 0;
    hard.memory[0x000a] = 0x29;

    // put r1, .less
    hard.memory[0x000b] = 0x03 << 2;
    hard.memory[0x000c] = 1 << 4;
    hard.memory[0x000d] = 0x1a;
    // brl r1
    hard.memory[0x000e] = 0x17 << 2;
    hard.memory[0x000f] = 1 << 4;

    // put r1, .equal
    hard.memory[0x0010] = 0x03 << 2;
    hard.memory[0x0011] = 1 << 4;
    hard.memory[0x0012] = 0x1f;
    // bre r1
    hard.memory[0x0013] = 0x18 << 2;
    hard.memory[0x0014] = 1 << 4;

    // put r1, .greater
    hard.memory[0x0015] = 0x03 << 2;
    hard.memory[0x0016] = 1 << 4;
    hard.memory[0x0017] = 0x24;
    // brg r1
    hard.memory[0x0018] = 0x19 << 2;
    hard.memory[0x0019] = 1 << 4;

    // .less
    // put r1, 0xff
    hard.memory[0x001a] = 0x03 << 2;
    hard.memory[0x001b] = 1 << 4;
    hard.memory[0x001c] = 0xff;
    // jmp r0
    hard.memory[0x001d] = 0x16 << 2;
    hard.memory[0x001e] = 0 << 4;

    // .equal
    // put r2, 0xff
    hard.memory[0x001f] = 0x03 << 2;
    hard.memory[0x0020] = 2 << 4;
    hard.memory[0x0021] = 0xff;
    // jmp r0
    hard.memory[0x0022] = 0x16 << 2;
    hard.memory[0x0023] = 0 << 4;

    // .greater
    // put r3, 0xff
    hard.memory[0x0024] = 0x03 << 2;
    hard.memory[0x0025] = 3 << 4;
    hard.memory[0x0026] = 0xff;
    // jmp r0
    hard.memory[0x0027] = 0x16 << 2;
    hard.memory[0x0028] = 0 << 4;

    // .finish
    // dbg
    hard.memory[0x0029] = 0x02 << 2;
    // hlt
    hard.memory[0x002a] = 0x01 << 2;
}

bool Sape::tick() {
    // parse first byte
    unsigned char byte = hard.memory[next_instr];
    unsigned char opcode = byte >> 2;
    unsigned char opflags = byte & 0b11;

    // execute
    instr::Result result = instr_registry[opcode]->exec(hard, next_instr, opflags);

    if (result.jump) {
        next_instr = result.addr;
    } else {
        next_instr += result.size;
    }

    return result.halt;
}

Sape::~Sape() {
    for (int i = 0; i < instr_number; i++) {
        delete instr_registry[i];
    }
    delete[] instr_registry;
}

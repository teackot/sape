#include "Cmp.h"

namespace instr {
    const char *Cmp::get_mnemonic() const {
        return "cmp";
    }

    Result Cmp::exec(Hardware &hard, unsigned short address, unsigned char opflags) {
        // parse register operands
        unsigned char regbyte = hard.memory[address + 1];
        unsigned short &ra = hard.registers[regbyte >> 4];
        unsigned short &rb = hard.registers[regbyte & 0x0F];

        if (ra < rb) {
            hard.flags.set_less();
        } else if (ra == rb) {
            hard.flags.set_equal();
        } else {
            hard.flags.set_greater();
        }

        return Result {
                false,
                2,
        };
    }
} // instr

#pragma once

#include "../Hardware.h"
#include "Result.h"

/*
 * Base class for all instructions
 */
namespace instr {
    class Instruction {
    public:
        virtual ~Instruction();

        [[nodiscard]] virtual const char *get_mnemonic() const = 0;

        virtual Result exec(Hardware &hard, unsigned short address, unsigned char opflags) = 0;
    };
} // instr

#pragma once

namespace instr {
    struct Result {
        bool halt = true;

        int size = 0;

        bool jump = false;
        unsigned short addr = 0;
    };
} // instr

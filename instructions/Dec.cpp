#include "Dec.h"

namespace instr {
    const char *Dec::get_mnemonic() const {
        return "dec";
    }

    Result Dec::exec(Hardware &hard, unsigned short address, unsigned char opflags) {
        // parse register operands
        unsigned char regbyte = hard.memory[address + 1];
        unsigned short &r = hard.registers[regbyte >> 4];

        r -= 1;

        return Result {
                false,
                2,
        };
    }
} // instr

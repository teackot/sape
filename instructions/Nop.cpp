#include "Nop.h"

#include <iostream>

namespace instr {
    const char *Nop::get_mnemonic() const {
        return "nop";
    }

    Result Nop::exec(Hardware &hard, unsigned short address, unsigned char opflags) {
        // do nothing
        std::cout << "DBG: nop" << std::endl;
        return Result {
            false,
            1,
        };
    }
} // instr
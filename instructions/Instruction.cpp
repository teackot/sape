//
// Created by teackot on 07/10/22.
//

#include "Instruction.h"

namespace instr {
    Instruction::~Instruction() = default;
} // instr
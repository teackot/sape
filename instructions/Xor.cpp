#include "Xor.h"

namespace instr {
    const char *Xor::get_mnemonic() const {
        return "xor";
    }

    Result Xor::exec(Hardware &hard, unsigned short address, unsigned char opflags) {
        // parse register operands
        unsigned char regbyte = hard.memory[address + 1];
        unsigned short &ra = hard.registers[regbyte >> 4];
        unsigned short &rb = hard.registers[regbyte & 0x0F];

        ra ^= rb;

        return Result {
                false,
                2,
        };
    }
} // instr

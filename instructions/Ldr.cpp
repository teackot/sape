#include "Ldr.h"

namespace instr {
    const char *Ldr::get_mnemonic() const {
        return "ldr";
    }

    Result Ldr::exec(Hardware &hard, unsigned short address, unsigned char opsize) {
        // parse register operands
        unsigned char regbyte = hard.memory[address + 1];
        unsigned short &ra = hard.registers[regbyte >> 4];
        unsigned short &rb = hard.registers[regbyte & 0x0F];

        switch (opsize) {
            case 0: // 8bit
                ra = hard.memory[rb];
                break;
            case 1: // 16bit
                ra = ((unsigned short)hard.memory[rb] << 8) | hard.memory[rb + 1];
                break;
            default:
                return Result {
                        true,
                        0,
                };
        }

        return Result {
                false,
                2,
        };
    }
} // instr

#include "Inc.h"

namespace instr {
    const char *Inc::get_mnemonic() const {
        return "inc";
    }

    Result Inc::exec(Hardware &hard, unsigned short address, unsigned char opflags) {
        // parse register operands
        unsigned char regbyte = hard.memory[address + 1];
        unsigned short &r = hard.registers[regbyte >> 4];

        r += 1;

        return Result {
                false,
                2,
        };
    }
} // instr

#include "Add.h"

namespace instr {
    const char *Add::get_mnemonic() const {
        return "add";
    }

    Result Add::exec(Hardware &hard, unsigned short address, unsigned char opflags) {
        // parse register operands
        unsigned char regbyte = hard.memory[address + 1];
        unsigned short &ra = hard.registers[regbyte >> 4];
        unsigned short &rb = hard.registers[regbyte & 0x0F];

        ra += rb;

        return Result {
                false,
                2
        };
    }
} // instr

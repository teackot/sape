#include "Push.h"

#include <iostream>
#include <iomanip>

namespace instr {
    const char *Push::get_mnemonic() const {
        return "psh";
    }

    Result Push::exec(Hardware &hard, unsigned short address, unsigned char opsize) {
        std::cerr
                << "0x"
                << std::hex << std::setw(4) << std::setfill('0')
                << address
                << "WRN: 'psh' instruction is not yet implemented"
                << std::endl;
        return Result {
                false,
                2,
        };
    }
} // instr

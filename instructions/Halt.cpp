//
// Created by teackot on 07/10/22.
//

#include "Halt.h"

#include <iostream>

namespace instr {
    const char *Halt::get_mnemonic() const {
        return "hlt";
    }

    Result Halt::exec(Hardware &hard, unsigned short address, unsigned char opflags) {
        // halt
        std::cout << "DBG: halting" << std::endl;
        return Result {
            true,
            1,
        };
    }
} // instr
#include "Shr.h"

namespace instr {
    const char *Shr::get_mnemonic() const {
        return "shr";
    }

    Result Shr::exec(Hardware &hard, unsigned short address, unsigned char opflags) {
        // parse register operands
        unsigned char regbyte = hard.memory[address + 1];
        unsigned short &r = hard.registers[regbyte >> 4];
        unsigned char   v = regbyte & 0x0F;

        r >>= v;

        return Result {
                false,
                2,
        };
    }
} // instr

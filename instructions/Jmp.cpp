#include "Jmp.h"

namespace instr {
    const char *Jmp::get_mnemonic() const {
        return "jmp";
    }

    Result Jmp::exec(Hardware &hard, unsigned short address, unsigned char opflags) {
        // parse register operands
        unsigned char regbyte = hard.memory[address + 1];
        unsigned short &r = hard.registers[regbyte >> 4];

        return Result {
                false,
                2,
                true,
                r,
        };
    }
} // instr

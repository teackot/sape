//
// Created by teackot on 07/10/22.
//

#include "Debug.h"

#include <iostream>
#include <iomanip>

namespace instr {
    const char *Debug::get_mnemonic() const {
        return "dbg";
    }

    Result Debug::exec(Hardware &hard, unsigned short address, unsigned char opflags) {
        // print debug info

        std::cout
            << "0x"
            << std::hex << std::setw(4) << std::setfill('0')
            << address
            << ": DBG: printing registers dump\n";
        for (int r = 0; r < 4; r++) {
            std::cout
                << std::dec << "| r" << r << " | " << "0x"
                << std::hex << std::setw(4) << std::setfill('0')
                << hard.registers[r] << " |\n";
        }
        std::cout << std::flush;

        return Result {
            false,
            1,
        };
    }
} // instr
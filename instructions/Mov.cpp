#include "Mov.h"

namespace instr {
    const char *Mov::get_mnemonic() const {
        return "mov";
    }

    Result Mov::exec(Hardware &hard, unsigned short address, unsigned char opsize) {
        // parse register operands
        unsigned char regbyte = hard.memory[address + 1];
        unsigned short &ra = hard.registers[regbyte >> 4];
        unsigned short &rb = hard.registers[regbyte & 0x0F];

        ra = rb;

//        switch (opsize) {
//            case 0: // 8bit
//                ra &= 0xFF00;
//                ra |= rb & 0x00FF;
//                break;
//            case 1:
//                ra = rb;
//                break;
//            default:
//                return Result {
//                    true,
//                    0,
//                };
//        }

        return Result {
            false,
            2,
        };
    }
} // instr
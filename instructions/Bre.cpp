#include "Bre.h"

namespace instr {
    const char *Bre::get_mnemonic() const {
        return "bre";
    }

    Result Bre::exec(Hardware &hard, unsigned short address, unsigned char inv) {
        // parse register operands
        unsigned char regbyte = hard.memory[address + 1];
        unsigned short &r = hard.registers[regbyte >> 4];
        auto &flags = hard.flags;

        bool condition;
        if (inv) { // !=
            condition = flags.get_less() || flags.get_greater();
        } else { // ==
            condition = flags.get_equal();
        }

        if (condition) { // jump
            return Result {
                    false,
                    2,
                    true,
                    r,
            };
        } else { // don't jump
            return Result {
                    false,
                    2,
                    false,
                    0,
            };
        }
    }
} // instr

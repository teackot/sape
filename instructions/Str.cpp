#include "Str.h"

namespace instr {
    const char *Str::get_mnemonic() const {
        return "str";
    }

    Result Str::exec(Hardware &hard, unsigned short address, unsigned char opsize) {
        // parse register operands
        unsigned char regbyte = hard.memory[address + 1];
        unsigned short &ra = hard.registers[regbyte >> 4];
        unsigned short &rb = hard.registers[regbyte & 0x0F];

        switch (opsize) {
            case 0: // 8bit
                hard.memory[rb] = ra & 0x00FF;
                break;
            case 1: // 16bit
                hard.memory[rb] = ra >> 8;
                hard.memory[rb + 1] = ra & 0x00FF;
                break;
            default:
                return Result {
                        true,
                        0,
                };
        }

        return Result {
                false,
                2,
        };
    }
} // instr

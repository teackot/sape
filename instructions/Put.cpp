#include "Put.h"

namespace instr {
    const char *Put::get_mnemonic() const {
        return "put";
    }

    Result Put::exec(Hardware &hard, unsigned short address, unsigned char opsize) {
        unsigned char r = (hard.memory[address + 1] & 0xF0) >> 4;
        switch (opsize) {
            case 0: // 8bit
                hard.registers[r] = hard.memory[address + 2];
                break;
            case 1: // 16bit
                hard.registers[r] = ((unsigned short)hard.memory[address + 2] << 8) | hard.memory[address + 3];
                break;
            default:
                return Result {
                    true,
                    0,
                };
        }

        return Result {
            false,
            opsize + 3,
        };
    }
} // instr
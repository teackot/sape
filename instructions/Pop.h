#pragma once

#include "Instruction.h"

namespace instr {
    class Pop : public Instruction {
    public:
        [[nodiscard]] const char *get_mnemonic() const override;

        Result exec(Hardware &hard, unsigned short address, unsigned char opsize) override;
    };
} // instr

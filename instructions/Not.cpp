#include "Not.h"

namespace instr {
    const char *Not::get_mnemonic() const {
        return "not";
    }

    Result Not::exec(Hardware &hard, unsigned short address, unsigned char opflags) {
        // parse register operands
        unsigned char regbyte = hard.memory[address + 1];
        unsigned short &r = hard.registers[regbyte >> 4];

        r = ~r;

        return Result {
                false,
                2,
        };
    }
} // instr

#include "Pop.h"

#include <iostream>
#include <iomanip>

namespace instr {
    const char *Pop::get_mnemonic() const {
        return "pop";
    }

    Result Pop::exec(Hardware &hard, unsigned short address, unsigned char opsize) {
        std::cerr
                << "0x"
                << std::hex << std::setw(4) << std::setfill('0')
                << address
                << "WRN: 'pop' instruction is not yet implemented"
                << std::endl;
        return Result {
                false,
                2,
        };
    }
} // instr

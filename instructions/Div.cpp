#include "Div.h"

namespace instr {
    const char *Div::get_mnemonic() const {
        return "div";
    }

    Result Div::exec(Hardware &hard, unsigned short address, unsigned char opflags) {
        // parse register operands
        unsigned char regbyte = hard.memory[address + 1];
        unsigned short &ra = hard.registers[regbyte >> 4];
        unsigned short &rb = hard.registers[regbyte & 0x0F];

        // readability shortcuts
        unsigned short &r0 = hard.registers[0];
        unsigned short &r1 = hard.registers[1];

        const auto va = ra,
                   vb = rb;

        r0 = va / vb;
        r1 = va % vb;

        return Result {
                false,
                2,
        };
    }
} // instr

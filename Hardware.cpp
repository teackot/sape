#include "Hardware.h"

Hardware::Hardware() {
    // init memory with zeros
    memory = new unsigned char [memory_size];
    for (int i = 0; i < memory_size; i++) {
        memory[i] = 0;
    }

    // init registers with zeros
    registers = new unsigned short [registers_number];
    for (int i = 0; i < registers_number; i++) {
        registers[i] = 0;
    }
}

Hardware::~Hardware() {
    delete[] memory;
    delete[] registers;
}

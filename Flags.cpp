#include "Flags.h"

bool Flags::get_less() {
    return less;
}

void Flags::set_less() {
    less = true;
    equal = false;
    greater = false;
}

bool Flags::get_equal() {
    return equal;
}

void Flags::set_equal() {
    less = false;
    equal = true;
    greater = false;
}

bool Flags::get_greater() {
    return greater;
}

void Flags::set_greater() {
    less = false;
    equal = false;
    greater = true;
}

void Flags::unset_ordering() {
    less = false;
    equal = false;
    greater = false;
}

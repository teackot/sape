#pragma once

#include "Hardware.h"
#include "instructions/Instruction.h"

class Sape {
private:
    Hardware hard;

    instr::Instruction **instr_registry;
    int instr_number;

    unsigned short next_instr;
public:
    Sape();
    ~Sape();

    bool tick();
};

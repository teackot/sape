#include <iostream>

#include "Sape.h"

int main() {
    Sape sape;
    while (not sape.tick());

    return 0;
}

# Sape (SRPE) - Simple RISC Processor Emulator

#### Instructions

opcode takes 6 bits

flags take 2 bits

Instructions list:

|  ✓  | opcode | mnemonic | flags | operands (size)       | description                               |
|:---:|--------|----------|-------|-----------------------|-------------------------------------------|
|  ✓  | 0x00   | nop      |       |                       | no operation                              |
|  ✓  | 0x01   | hlt      |       |                       | halt                                      |
|  ✓  | 0x02   | dbg      |       |                       | DEBUG: print all registers                |
|  ✓  | 0x03   | put      | size  | reg(4)  , const(8/16) | reg = const                               |
|  ✓  | 0x04   | mov      |       | reg_a(4), reg_b(4)    | reg_a = reg_b                             |
|  ✓  | 0x05   | ldr      | size  | reg_a(4), reg16_b(4)  | reg_a = [reg16_b]                         |
|  ✓  | 0x06   | str      | size  | reg_a(4), reg16_b(4)  | [reg16_b] = reg_a                         |
|     | 0x07   | psh      | size  | reg(4)                | stack <- reg                              |
|     | 0x08   | pop      | size  | reg(4)                | stack -> reg                              |
|  ✓  | 0x09   | add      |       | reg_a(4), reg_b(4)    | reg_a += reg_b                            |
|  ✓  | 0x0a   | sub      |       | reg_a(4), reg_b(4)    | reg_a -= reg_b                            |
|  ✓  | 0x0b   | mul      |       | reg_a(4), reg_b(4)    | reg_a *= reg_b                            |
|  ✓  | 0x0c   | div      |       | reg_a(4), reg_b(4)    | r0 = reg_a // reg_b; r1 = reg_a % reg_b   |
|  ✓  | 0x0d   | inc      |       | reg(4)                | reg += 1                                  |
|  ✓  | 0x0e   | dec      |       | reg(4)                | reg -= 1                                  |
|  ✓  | 0x0f   | shl      |       | reg(4)  , const(4)    | reg <<= const                             |
|  ✓  | 0x10   | shr      |       | reg(4)  , const(4)    | reg >>= const                             |
|  ✓  | 0x11   | and      |       | reg_a(4), reg_b(4)    | reg_a &= reg_b                            |
|  ✓  | 0x12   | or       |       | reg_a(4), reg_b(4)    | reg_a or= reg_b                           |
|  ✓  | 0x13   | xor      |       | reg_a(4), reg_b(4)    | reg_a ^= reg_b                            |
|  ✓  | 0x14   | not      |       | reg(4)                | reg = ~reg                                |
|  ✓  | 0x15   | cmp      |       | reg_a(4), reg_b(4)    | compare reg_a, reg_b                      |
|  ✓  | 0x16   | jmp      |       | reg(4)                | jump to the address stored in reg         |
|  ✓  | 0x17   | brl      | inv   | reg(4)                | branch to the address stored in reg if <  |
|  ✓  | 0x18   | bre      | inv   | reg(4)                | branch to the address stored in reg if == |
|  ✓  | 0x19   | brg      | inv   | reg(4)                | branch to the address stored in reg if >  |

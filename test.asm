    put r0, 2
    put r1, 1
    cmp r0, r1

    put r0, .finish

    put r1, .less
    brl r1

    put r1, .equal
    bre r1

    put r1, .greater
    brg r1

.less
    put r1, 0xff
    jmp r0
.equal
    put r2, 0xff
    jmp r0
.greater
    put r3, 0xff
    jmp r0

.finish
    dbg
    hlt

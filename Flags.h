#pragma once

class Flags {
private:
    bool less    = false;
    bool equal   = false;
    bool greater = false;

public:
    bool get_less();
    void set_less();

    bool get_equal();
    void set_equal();

    bool get_greater();
    void set_greater();

    void unset_ordering();
};
